import React, { Component } from 'react';
import Services from "../services/Services.js";

class UsecaseForm extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.handleClose = this.handleClose.bind(this);
        this.setModalRef = this.setModalRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.state = {
            
        };
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event) {
        if (this.modalRef && !this.modalRef.contains(event.target)) {
            this.handleClose();
        }
    }

    handleClose() {
        this.props.onClose();
    }

    setModalRef(node) {
        this.modalRef = node;
    }


    render() {
        return (
            <div className="clusterInfoDiv">
                <div className="modal-screen"></div>
                <div className="hidden-sm-down position-relative" style={{ "minHeight": 600 }}>
                    <div className="modal" style={{ top: "7%" }}>
                        <div className="container" ref={this.setModalRef}>
                            <div className="mlp-modal-header dls-accent-blue-02-bg dls-accent-white-01">
                                <div className="mlp-modal-heading">
                                    <h2 className="mlp-no-margin pad-std heading-3">Create new Usecase</h2>
                                </div>
                                <div className="pad-std mlp-modal-close">
                                    <image onClick={this.handleClose}
                                        data-toggle="tooltip-auto" title="Close"
                                        className="mlp-cursor-pointer dls-glyph-close"></image>
                                </div>
                            </div>
                            <div className="usecase-form">
                                {/* name */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="name">Name</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="text" id="name" className="form-control" />
                                    </div>
                                </div>

                                {/* description */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="description">Description</label>
                                    </div>
                                    <div className="col-md-4">
                                        <textarea rows="3" id="description" className="form-control" ></textarea>
                                    </div>
                                </div>

                                {/* email */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="email">Email</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="text" id="email" className="form-control" />
                                    </div>
                                </div>

                                {/* Application ID / AIM ID / CAR ID  */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="app_id">Application ID / AIM ID / CAR ID</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="text" id="app_id" className="form-control" />
                                    </div>
                                </div>

                                {/* On-boarding status */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="status">Status</label>
                                    </div>
                                    <div className="col-md-4">
                                        <select id="status" className="form-control">
                                            <option value="ON-BOARDED">on-boarded</option>
                                            <option value="IN-PROGRESS">in-progress</option>
                                            <option value="NOT-STARTED">not-started</option>
                                        </select>
                                    </div>
                                </div>

                                {/* AWS PRC */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="aws_prc">AWS PRC</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="text" id="aws_prc" className="form-control" />
                                    </div>
                                </div>

                                {/* environemnt */}
                                <div className="row">
                                    <div className="col-md-4">
                                        <label for="environemnt">Environment</label>
                                    </div>
                                    <div className="col-md-4">
                                        <select id="environment" className="form-control">
                                            <option value="AWS">AWS</option>
                                            <option value="PRC">PRC</option>
                                            <option value="BOTH">BOTH</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-2"></div>
                                    <div className="col-md-4">
                                        <button className="btn btn-primary">Submit</button>
                                    </div>
                                    <div className="col-md-4">
                                        <button className="btn btn-primary">Cancel</button>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div>

                            </div>
                       </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UsecaseForm;

// Use Cases -> Create New

// Once clicked on "Create New", a form will come up with the following fields:

// 1. Name of the Use-Case 
//  2. Description of the use-case.
//  3. E-mail address of users (Should be able to pull by typing name and connecting to active directory).
// 4. Application ID / AIM ID / CAR ID 
// 5. On-boarding status (on-boarded, in-progress, not-started – should have a pop-up for detailed status)
// 6. AWS PRC Group for the users. 
// 7. Environment (AWS/ECP/Both)

// Once saved, it should be stored in the list of existing use-cases
